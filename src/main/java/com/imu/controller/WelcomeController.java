package com.imu.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;

@Controller
public class WelcomeController {
    @RequestMapping("/index")
    public String welcomeHtml(HashMap map) {
        map.put("parameter", "欢迎进入 HTML 页面");
        return "/index";
    }
}
