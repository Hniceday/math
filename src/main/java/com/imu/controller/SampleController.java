package com.imu.controller;

import com.imu.biz.Distribution;
import com.imu.biz.DistributionFactory;
import com.imu.pojo.DistributionRequest;
import com.imu.util.Linspace;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class SampleController {

    private Linspace linspace = new Linspace();

    @PostMapping("/generateSample")
    public List<List<Double>> generateSample(@RequestBody DistributionRequest request) {
        // 根据请求生成样本点
        Distribution distribution = DistributionFactory.createDistribution(request);
        List<Double> samples = distribution.generateSamples(request.getSampleSize());
        double min = Collections.min(samples);
        double max = Collections.max(samples);
        List<Double> xValues = linspace.linspace(min, max,100);
        List<Double> pdfValuesTrue = distribution.probabilityDensityFunction(xValues);
        List<Double> cdfValuesTrue = distribution.cumulativeDistributionFunction(xValues);
        List<Double> pdfValuesEstimate = distribution.estimateProbabilityDensityFunction(xValues);
        List<Double> cdfValuesEstimate = distribution.estimateCumulativeDistributionFunction(xValues);

        Map<String, List<Double>> cdfResult = simulationCDF(samples);

        // 获取结果
//        List<Double> sortedSamples = cdfResult.get("sortedSamples");
//        List<Double> simCDF = cdfResult.get("cdfValues");


        List<List<Double>> result = new ArrayList<>();
        result.add(samples);
        result.add(pdfValuesTrue);
        result.add(cdfValuesTrue);
        result.add(xValues);
        result.add(pdfValuesEstimate);
        result.add(cdfValuesEstimate);



        return result;
    }

    public static Map<String, List<Double>> simulationCDF(List<Double> samples) {
        // 对样本进行排序
        List<Double> sortedSamples = new ArrayList<>(samples);
        Collections.sort(sortedSamples);

        // 计算累积分布函数（CDF）
        List<Double> cdfValues = new ArrayList<>();
        double sampleSize = sortedSamples.size();
        for (int i = 0; i < sampleSize; i++) {
            double cumulativeProbability = (i + 1) / sampleSize;
            cdfValues.add(cumulativeProbability);
        }

        // 将排序后的样本和对应的CDF一起存放到Map中
        Map<String, List<Double>> result = new HashMap<>();
        result.put("sortedSamples", sortedSamples);
        result.put("cdfValues", cdfValues);

        return result;
    }

}

