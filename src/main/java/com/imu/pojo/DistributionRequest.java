package com.imu.pojo;

import java.util.List;

// DistributionRequest.java
public class DistributionRequest {
    private String distributionType;
    private int parameterCount;
    private List<Double> parameters;
    private int sampleSize;

    public String getDistributionType() {
        return distributionType;
    }

    public void setDistributionType(String distributionType) {
        this.distributionType = distributionType;
    }

    public int getParameterCount() {
        return parameterCount;
    }

    public void setParameterCount(int parameterCount) {
        this.parameterCount = parameterCount;
    }

    public List<Double> getParameters() {
        return parameters;
    }

    public void setParameters(List<Double> parameters) {
        this.parameters = parameters;
    }

    public int getSampleSize() {
        return sampleSize;
    }

    public void setSampleSize(int sampleSize) {
        this.sampleSize = sampleSize;
    }

    @Override
    public String toString() {
        return "DistributionRequest{" +
                "distributionType='" + distributionType + '\'' +
                ", parameterCount=" + parameterCount +
                ", parameters=" + parameters +
                ", sampleSize=" + sampleSize +
                '}';
    }
}

