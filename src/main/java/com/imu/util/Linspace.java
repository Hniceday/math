package com.imu.util;

import java.util.ArrayList;
import java.util.List;

public class Linspace {

    public Linspace(){

    }
    public List<Double> linspace(double start, double end, int numPoints) {
        List<Double> result = new ArrayList<>();
        double step = (end - start) / (numPoints - 1);
        for (int i = 0; i < numPoints; i++) {
            result.add(start + i * step);
        }

        return result;
    }
}
