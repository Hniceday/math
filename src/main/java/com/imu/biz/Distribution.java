package com.imu.biz;

import java.util.List;

public interface Distribution {
    // 生成指定函数的样本点
    List<Double> generateSamples(int sampleSize);
    // 得到该函数真实的的PDF
    List<Double> probabilityDensityFunction(List<Double> xLabels);
    // 得到该函数真实的的CDF
    List<Double> cumulativeDistributionFunction(List<Double> xLabels);
    // 得到该函数估计的的PDF
    List<Double> estimateProbabilityDensityFunction(List<Double> xLabels);
    // 得到该函数估计的的CDF
    List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels);
}
