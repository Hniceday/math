package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ExponentialDistributionGenerator implements Distribution {

    private double lambda;
    private double lambdaEstimate;

    public ExponentialDistributionGenerator(double lambda) {
        this.lambda = lambda;
    }

    // 生成样本点
    @Override
    public List<Double> generateSamples(int sampleSize) {
        double[] data = new double[sampleSize];
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            samples.add(-Math.log(Math.random()) / lambda);
            data[i] = samples.get(i);
        }
        // 根据样本点估计出他的参数lambda
        this.lambdaEstimate = estimateLambda(data);
        System.out.println(this.lambdaEstimate);
        return samples;
    }

    // 理论的pdf
    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = lambda * Math.exp(-lambda * x);
            pdfValues.add(pdf);
        }
        return pdfValues;
    }

    // 理论的cdf
    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double cumulativeProbability = 1 - Math.exp(-lambda * x);
            cdfValues.add(cumulativeProbability);
        }
        return cdfValues;
    }

    // 使用估计的lambda计算出的pdf，xLabels 表示0-20分成1000个区间
    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValuesEstimate = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = lambda * Math.exp(-this.lambdaEstimate * x);
            pdfValuesEstimate.add(pdf);
        }
        return pdfValuesEstimate;
    }

    //// 使用估计的lambda计算出的cdf
    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValuesEstimate = new ArrayList<>();
        for (Double x : xLabels) {
            double cumulativeProbability = 1 - Math.exp(-this.lambdaEstimate * x);
            cdfValuesEstimate.add(cumulativeProbability);
        }
        return cdfValuesEstimate;
    }

    // 根据样本点估计参数lambda
    private static double estimateLambda(double[] data) {
        double sum = Arrays.stream(data).sum();
        return 1.0 / (sum / data.length);
    }

}
