package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PoissonDistributionGenerator implements Distribution {

    private double lambda;  // 平均发生率参数

    public PoissonDistributionGenerator(double lambda) {
        if (lambda <= 0) {
            throw new IllegalArgumentException("Lambda parameter must be positive.");
        }
        this.lambda = lambda;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            // Poisson 分布的生成公式
            double L = Math.exp(-lambda);
            double p = 1.0;
            int k = 0;

            do {
                k++;
                p *= Math.random();
            } while (p > L);

            samples.add((double) (k - 1));
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            if (x < 0 || x != Math.floor(x)) {
                pdfValues.add(0.0);  // Poisson 分布在负数和小数的概率为零
            } else {
                double pdf = Math.exp(-lambda) * Math.pow(lambda, x) / factorial(x);
                pdfValues.add(pdf);
            }
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        double cumulativeProbability = 0;
        for (Double x : xLabels) {
            if (x < 0 || x != Math.floor(x)) {
                cumulativeProbability = 0.0;  // Poisson 分布在负数和小数的累积概率为零
            } else {
                cumulativeProbability += probabilityDensityFunction(Arrays.asList(x)).get(0);
            }
            cdfValues.add(cumulativeProbability);
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        // Poisson 分布是已知参数的分布，不需要进行估算
        return probabilityDensityFunction(xLabels);
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        // Poisson 分布是已知参数的分布，不需要进行估算
        return cumulativeDistributionFunction(xLabels);
    }

    private double factorial(double n) {
        // 计算阶乘的递归函数
        if (n == 0) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }
}
