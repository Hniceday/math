package com.imu.biz.impl;

import com.imu.biz.Distribution;
import org.apache.commons.math3.distribution.ConstantRealDistribution;

import java.util.ArrayList;
import java.util.List;

public class ConstantRealDistributionGenerator implements Distribution {

    private double constantValue;

    public ConstantRealDistributionGenerator(double constantValue) {
        this.constantValue = constantValue;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        ConstantRealDistribution constantRealDistribution = new ConstantRealDistribution(constantValue);
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            samples.add(constantRealDistribution.sample());
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = (x.equals(constantValue)) ? Double.POSITIVE_INFINITY : 0.0;
            pdfValues.add(pdf);
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double cumulativeProbability = (x >= constantValue) ? 1.0 : 0.0;
            cdfValues.add(cumulativeProbability);
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        // Since ConstantRealDistribution has a constant probability density, there is no need for estimation.
        return probabilityDensityFunction(xLabels);
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        // Since ConstantRealDistribution has a constant cumulative distribution, there is no need for estimation.
        return cumulativeDistributionFunction(xLabels);
    }
}
