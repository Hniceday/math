package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GammaDistributionGenerator implements Distribution {

    private double shape;  // 形状参数
    private double scale;  // 尺度参数

    public GammaDistributionGenerator(double shape, double scale) {
        if (shape <= 0 || scale <= 0) {
            throw new IllegalArgumentException("Shape and scale parameters must be positive.");
        }
        this.shape = shape;
        this.scale = scale;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            double sample = generateSingleSample();
            samples.add(sample);
        }
        return samples;
    }

    private double generateSingleSample() {
        // 使用 Marsaglia and Tsang 方法生成 Gamma 分布的样本
        double d, c, x, v, u;

        d = shape - 1.0 / 3.0;
        c = 1.0 / Math.sqrt(9 * d);

        do {
            do {
                x = generateStandardNormalSample();
                v = 1.0 + c * x;
            } while (v <= 0);

            v = v * v * v;
            u = Math.random();
        } while (u >= 1 - 0.0331 * x * x * x * x &&
                Math.log(u) >= 0.5 * x * x + d * (1 - v + Math.log(v)));

        return d * v * scale;
    }

    private double generateStandardNormalSample() {
        // 使用 Box-Muller 变换生成标准正态分布的样本
        double u1 = Math.random();
        double u2 = Math.random();
        return Math.sqrt(-2.0 * Math.log(u1)) * Math.cos(2.0 * Math.PI * u2);
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = Math.pow(x, shape - 1) * Math.exp(-x / scale) / (Math.pow(scale, shape) * gammaFunction(shape));
            pdfValues.add(pdf);
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        double cumulativeProbability = 0;
        for (Double x : xLabels) {
            cumulativeProbability += probabilityDensityFunction(Arrays.asList(x)).get(0);
            cdfValues.add(cumulativeProbability);
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        // Gamma 分布是已知参数的分布，不需要进行估算
        return probabilityDensityFunction(xLabels);
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        // Gamma 分布是已知参数的分布，不需要进行估算
        return cumulativeDistributionFunction(xLabels);
    }

    private double gammaFunction(double x) {
        // 伽玛函数的近似计算
        double coef[] = {76.18009172947146, -86.50532032941677, 24.01409824083091,
                -1.231739572450155, 0.1208650973866179e-2, -0.5395239384953e-5};
        double y = x;
        double tmp = x + 5.5;
        tmp -= (x + 0.5) * Math.log(tmp);
        double ser = 1.000000000190015;

        for (int j = 0; j < 6; j++) {
            y++;
            ser += coef[j] / y;
        }

        return -tmp + Math.log(2.5066282746310005 * ser / x);
    }
}
