package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParetoDistributionGenerator implements Distribution {

    private double shape;  // 形状参数
    private double min;    // 最小值参数

    public ParetoDistributionGenerator(double shape, double min) {
        if (shape <= 0 || min <= 0) {
            throw new IllegalArgumentException("Shape and min parameters must be positive.");
        }
        this.shape = shape;
        this.min = min;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            // Pareto 分布的生成公式
            double u = Math.random();
            double sample = min * Math.pow(u, -1 / shape);
            samples.add(sample);
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            if (x < min) {
                pdfValues.add(0.0);  // Pareto 分布在最小值以下的概率为零
            } else {
                double pdf = (shape / Math.pow(min, shape)) * Math.pow(x, -1 - shape);
                pdfValues.add(pdf);
            }
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            if (x < min) {
                cdfValues.add(0.0);  // Pareto 分布在最小值以下的累积概率为零
            } else {
                double cdf = 1 - Math.pow(min / x, shape);
                cdfValues.add(cdf);
            }
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        // Pareto 分布是已知参数的分布，不需要进行估算
        return probabilityDensityFunction(xLabels);
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        // Pareto 分布是已知参数的分布，不需要进行估算
        return cumulativeDistributionFunction(xLabels);
    }
}
