package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.List;

public class UniformIntegerDistributionGenerator implements Distribution {

    private int lower;
    private int upper;

    public UniformIntegerDistributionGenerator(int lower, int upper) {
        this.lower = lower;
        this.upper = upper;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            samples.add((double) generateSingleSample());
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        return null;
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        return null;
    }

    private int generateSingleSample() {
        return lower + (int) (Math.random() * (upper - lower + 1));
    }
}

