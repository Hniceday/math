package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.List;

public class EnumeratedDistributionGenerator implements Distribution {

    private List<Double> values;
    private List<Double> probabilities;

    public EnumeratedDistributionGenerator(List<Double> values, List<Double> probabilities) {
        this.values = values;
        this.probabilities = probabilities;

        // 检查输入是否有效
        if (values.size() != probabilities.size()) {
            throw new IllegalArgumentException("Values and probabilities lists must have the same size.");
        }

        // 检查概率和是否为正数
        for (double probability : probabilities) {
            if (probability < 0) {
                throw new IllegalArgumentException("Probabilities must be non-negative.");
            }
        }
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            double randomValue = Math.random();
            double cumulativeProbability = 0;

            // 根据累积概率找到对应的值
            for (int j = 0; j < values.size(); j++) {
                cumulativeProbability += probabilities.get(j);
                if (randomValue <= cumulativeProbability) {
                    samples.add(values.get(j));
                    break;
                }
            }
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        // 在枚举分布中，概率密度函数通常是给定值的概率
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            if (values.contains(x)) {
                int index = values.indexOf(x);
                pdfValues.add(probabilities.get(index));
            } else {
                pdfValues.add(0.0); // 如果 x 不在值的列表中，概率为零
            }
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        // 在枚举分布中，累积分布函数是给定值的累积概率
        List<Double> cdfValues = new ArrayList<>();
        double cumulativeProbability = 0;
        for (Double x : xLabels) {
            if (values.contains(x)) {
                int index = values.indexOf(x);
                cumulativeProbability += probabilities.get(index);
            }
            cdfValues.add(cumulativeProbability);
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        // 在枚举分布中，不需要估算参数，直接返回概率密度函数
        return probabilityDensityFunction(xLabels);
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        // 在枚举分布中，不需要估算参数，直接返回累积分布函数
        return cumulativeDistributionFunction(xLabels);
    }
}
