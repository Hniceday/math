package com.imu.biz.impl;

import com.imu.biz.Distribution;
import org.apache.commons.math3.distribution.ChiSquaredDistribution;
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.SimplexOptimizer;

import java.util.ArrayList;
import java.util.List;

public class ChiSquaredDistributionGenerator implements Distribution {

    private double degreesOfFreedom;
    private double degreesOfFreedomEstimate;

    public ChiSquaredDistributionGenerator(double degreesOfFreedom) {
        this.degreesOfFreedom = degreesOfFreedom;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        ChiSquaredDistribution chiSquaredDistribution = new ChiSquaredDistribution(degreesOfFreedom);
        double[] data = new double[sampleSize];
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            samples.add(chiSquaredDistribution.sample());
            data[i] = samples.get(i);
        }
        this.degreesOfFreedomEstimate = estimateDegreesOfFreedom(data);
        System.out.println(this.degreesOfFreedomEstimate);
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = new ChiSquaredDistribution(degreesOfFreedom).density(x);
            pdfValues.add(pdf);
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double cumulativeProbability = new ChiSquaredDistribution(degreesOfFreedom).cumulativeProbability(x);
            cdfValues.add(cumulativeProbability);
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValuesEstimate = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = new ChiSquaredDistribution(degreesOfFreedomEstimate).density(x);
            pdfValuesEstimate.add(pdf);
        }
        return pdfValuesEstimate;
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValuesEstimate = new ArrayList<>();
        for (Double x : xLabels) {
            double cumulativeProbability = new ChiSquaredDistribution(degreesOfFreedomEstimate).cumulativeProbability(x);
            cdfValuesEstimate.add(cumulativeProbability);
        }
        return cdfValuesEstimate;
    }

    private double estimateDegreesOfFreedom(double[] data) {
        double sum = 0;
        for (double value : data) {
            sum += value;
        }
        return sum / data.length;  // A simple estimation, you may use a more sophisticated method if needed.
    }
}
