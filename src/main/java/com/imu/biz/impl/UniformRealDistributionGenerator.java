package com.imu.biz.impl;

// UniformRealDistribution.java
import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.List;

public class UniformRealDistributionGenerator implements Distribution {

    private double lower;
    private double upper;

    public UniformRealDistributionGenerator(double lower, double upper) {
        this.lower = lower;
        this.upper = upper;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            double sample = lower + Math.random() * (upper - lower);
            samples.add(sample);
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        return null;
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        return null;
    }
}

