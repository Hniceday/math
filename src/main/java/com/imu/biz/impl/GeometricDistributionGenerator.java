package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GeometricDistributionGenerator implements Distribution {

    private double successProbability;  // 成功的概率

    public GeometricDistributionGenerator(double successProbability) {
        if (successProbability <= 0 || successProbability >= 1) {
            throw new IllegalArgumentException("Success probability must be between 0 and 1 exclusive.");
        }
        this.successProbability = successProbability;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            double sample = generateSingleSample();
            samples.add(sample);
        }
        return samples;
    }

    private double generateSingleSample() {
        // 生成几何分布的样本
        double sample = 1;  // 初始化为1，因为至少需要一次尝试
        while (Math.random() > successProbability) {
            sample++;
        }
        return sample;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            if (x < 1) {
                pdfValues.add(0.0);  // 几何分布在1及以下的概率为零
            } else {
                double pdf = successProbability * Math.pow(1 - successProbability, x - 1);
                pdfValues.add(pdf);
            }
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        double cumulativeProbability = 0;
        for (Double x : xLabels) {
            if (x < 1) {
                cumulativeProbability = 0.0;  // 几何分布在1及以下的累积概率为零
            } else {
                cumulativeProbability += probabilityDensityFunction(Arrays.asList(x)).get(0);
            }
            cdfValues.add(cumulativeProbability);
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        // 几何分布是已知参数的分布，不需要进行估算
        return probabilityDensityFunction(xLabels);
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        // 几何分布是已知参数的分布，不需要进行估算
        return cumulativeDistributionFunction(xLabels);
    }
}
