package com.imu.biz.impl;

import com.imu.biz.Distribution;
import org.apache.commons.math3.distribution.FDistribution;
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.SimplexOptimizer;

import java.util.ArrayList;
import java.util.List;

public class FDistributionGenerator implements Distribution {

    private double numeratorDegreesOfFreedom;
    private double denominatorDegreesOfFreedom;
    private double numeratorDegreesOfFreedomEstimate;
    private double denominatorDegreesOfFreedomEstimate;

    public FDistributionGenerator(double numeratorDegreesOfFreedom, double denominatorDegreesOfFreedom) {
        this.numeratorDegreesOfFreedom = numeratorDegreesOfFreedom;
        this.denominatorDegreesOfFreedom = denominatorDegreesOfFreedom;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        FDistribution fDistribution = new FDistribution(numeratorDegreesOfFreedom, denominatorDegreesOfFreedom);
        double[] data = new double[sampleSize];
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            samples.add(fDistribution.sample());
            data[i] = samples.get(i);
        }
        double[] estimates = estimateDegreesOfFreedom(data);
        this.numeratorDegreesOfFreedomEstimate = estimates[0];
        this.denominatorDegreesOfFreedomEstimate = estimates[1];
        System.out.println(this.numeratorDegreesOfFreedomEstimate + "," + this.denominatorDegreesOfFreedomEstimate);
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = new FDistribution(numeratorDegreesOfFreedom, denominatorDegreesOfFreedom).density(x);
            pdfValues.add(pdf);
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double cumulativeProbability = new FDistribution(numeratorDegreesOfFreedom, denominatorDegreesOfFreedom).cumulativeProbability(x);
            cdfValues.add(cumulativeProbability);
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValuesEstimate = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = new FDistribution(numeratorDegreesOfFreedomEstimate, denominatorDegreesOfFreedomEstimate).density(x);
            pdfValuesEstimate.add(pdf);
        }
        return pdfValuesEstimate;
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValuesEstimate = new ArrayList<>();
        for (Double x : xLabels) {
            double cumulativeProbability = new FDistribution(numeratorDegreesOfFreedomEstimate, denominatorDegreesOfFreedomEstimate).cumulativeProbability(x);
            cdfValuesEstimate.add(cumulativeProbability);
        }
        return cdfValuesEstimate;
    }


    private double[] estimateDegreesOfFreedom(double[] data) {
        ObjectiveFunction objective = new ObjectiveFunction(params -> {
            double numerator = params[0];
            double denominator = params[1];
            double sumLogLikelihood = 0.0;
            FDistribution fDistribution = new FDistribution(numerator, denominator);
            for (double value : data) {
                sumLogLikelihood += Math.log(fDistribution.density(value));
            }
            return -sumLogLikelihood;
        });

        SimplexOptimizer optimizer = new SimplexOptimizer(1e-6, 1e-8);
        PointValuePair optimum = optimizer.optimize(
                new MaxEval(1000),
                GoalType.MINIMIZE,
                objective,
                new InitialGuess(new double[]{numeratorDegreesOfFreedom, denominatorDegreesOfFreedom})
        );

        return optimum.getPoint();
    }
}

