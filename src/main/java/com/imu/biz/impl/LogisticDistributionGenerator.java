package com.imu.biz.impl;

import com.imu.biz.Distribution;
import org.apache.commons.math3.distribution.LogisticDistribution;

import java.util.ArrayList;
import java.util.List;

public class LogisticDistributionGenerator implements Distribution {

    private double location;
    private double scale;

    public LogisticDistributionGenerator(double location, double scale) {
        this.location = location;
        this.scale = scale;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        LogisticDistribution logisticDistribution = new LogisticDistribution(location, scale);

        for (int i = 0; i < sampleSize; i++) {
            samples.add(logisticDistribution.sample());
        }

        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = new LogisticDistribution(location, scale).density(x);
            pdfValues.add(pdf);
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double cumulativeProbability = new LogisticDistribution(location, scale).cumulativeProbability(x);
            cdfValues.add(cumulativeProbability);
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        // Logistic Distribution does not have parameters to estimate.
        return probabilityDensityFunction(xLabels);
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        // Logistic Distribution does not have parameters to estimate.
        return cumulativeDistributionFunction(xLabels);
    }
}


