package com.imu.biz.impl;
import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.List;

public class BinomialDistributionGenerator implements Distribution {

    private int numberOfTrials;
    private double probabilityOfSuccess;

    public BinomialDistributionGenerator(int numberOfTrials, double probabilityOfSuccess) {
        this.numberOfTrials = numberOfTrials;
        this.probabilityOfSuccess = probabilityOfSuccess;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            int successes = 0;
            for (int j = 0; j < numberOfTrials; j++) {
                if (Math.random() < probabilityOfSuccess) {
                    successes++;
                }
            }
            samples.add((double) successes);
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        return null;
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        return null;
    }
}
