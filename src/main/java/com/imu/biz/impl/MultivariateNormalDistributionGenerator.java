package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.List;

import java.util.ArrayList;
import java.util.List;

public class MultivariateNormalDistributionGenerator implements Distribution {

    private double[] mean;
    private double[][] covarianceMatrix;

    public MultivariateNormalDistributionGenerator(double[] mean, double[][] covarianceMatrix) {
        this.mean = mean;
        this.covarianceMatrix = covarianceMatrix;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            samples.addAll(generateSingleSample());
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        return null;
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        return null;
    }

    private List<Double> generateSingleSample() {
        int dimension = mean.length;
        List<Double> sample = new ArrayList<>();

        // Generate uncorrelated standard normal random variables
        List<Double> z = new ArrayList<>();
        for (int i = 0; i < dimension; i++) {
            double u1 = Math.random();
            double u2 = Math.random();
            double randStdNormal = Math.sqrt(-2.0 * Math.log(u1)) * Math.sin(2.0 * Math.PI * u2);
            z.add(randStdNormal);
        }

        // Multiply by the Cholesky decomposition of the covariance matrix
        for (int i = 0; i < dimension; i++) {
            double sum = mean[i];
            for (int j = 0; j <= i; j++) {
                sum += covarianceMatrix[i][j] * z.get(j);
            }
            sample.add(sum);
        }

        return sample;
    }
}
