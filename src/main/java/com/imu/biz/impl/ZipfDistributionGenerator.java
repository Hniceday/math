package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.List;

public class ZipfDistributionGenerator implements Distribution {

    private int numberOfElements;
    private double exponent;

    public ZipfDistributionGenerator(int numberOfElements, double exponent) {
        this.numberOfElements = numberOfElements;
        this.exponent = exponent;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            samples.add(generateSingleSample());
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        return null;
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        return null;
    }

    private Double generateSingleSample() {
        double sum = 0.0;
        for (int i = 1; i <= numberOfElements; i++) {
            sum += 1.0 / Math.pow(i, exponent);
        }
        double r = Math.random() * sum;
        sum = 0.0;
        for (int i = 1; i <= numberOfElements; i++) {
            sum += 1.0 / Math.pow(i, exponent);
            if (sum >= r) {
                return (double) i;
            }
        }
        // Fallback
        return (double) numberOfElements;
    }
}

