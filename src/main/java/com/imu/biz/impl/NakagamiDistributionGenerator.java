package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.List;

public class NakagamiDistributionGenerator implements Distribution {

    private double mu;
    private double omega;

    public NakagamiDistributionGenerator(double mu, double omega) {
        this.mu = mu;
        this.omega = omega;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            samples.add(generateSingleSample());
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        return null;
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        return null;
    }

    private Double generateSingleSample() {
        double u = Math.random();
        return omega * Math.sqrt(Math.pow(-2.0 * Math.log(u), 2.0 / mu));
    }
}

