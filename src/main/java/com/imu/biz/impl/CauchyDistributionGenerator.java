package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CauchyDistributionGenerator implements Distribution {

    private double location; // Location parameter
    private double scale; // Scale parameter

    public CauchyDistributionGenerator(double location, double scale) {
        this.location = location;
        this.scale = scale;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            double u1 = Math.random();
            double u2 = Math.random();

            // Box-Muller transform to generate standard Cauchy distributed sample
            double z = location + scale * Math.tan(Math.PI * (u1 - 0.5));

            samples.add(z);
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = (scale / (Math.PI * (scale * scale + (x - location) * (x - location))));
            pdfValues.add(pdf);
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        double cumulativeProbability = 0;
        for (Double x : xLabels) {
            cumulativeProbability += probabilityDensityFunction(Arrays.asList(x)).get(0);
            cdfValues.add(cumulativeProbability);
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        // Cauchy distribution is non-parametric, so we don't estimate parameters here
        return probabilityDensityFunction(xLabels);
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        // Cauchy distribution is non-parametric, so we don't estimate parameters here
        return cumulativeDistributionFunction(xLabels);
    }
}
