package com.imu.biz.impl;
import com.imu.biz.Distribution;
import org.apache.commons.math3.distribution.HypergeometricDistribution;

import java.util.ArrayList;
import java.util.List;

public class HypergeometricDistributionGenerator implements Distribution {

    private int populationSize;
    private int numberOfSuccesses;
    private int sampleSize;

    public HypergeometricDistributionGenerator(int populationSize, int numberOfSuccesses, int sampleSize) {
        this.populationSize = populationSize;
        this.numberOfSuccesses = numberOfSuccesses;
        this.sampleSize = sampleSize;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        HypergeometricDistribution hypergeometricDistribution = new HypergeometricDistribution(populationSize, numberOfSuccesses, sampleSize);

        for (int i = 0; i < sampleSize; i++) {
            samples.add((double) hypergeometricDistribution.sample());
        }

        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = new HypergeometricDistribution(populationSize, numberOfSuccesses, sampleSize).probability(x.intValue());
            pdfValues.add(pdf);
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double cumulativeProbability = new HypergeometricDistribution(populationSize, numberOfSuccesses, sampleSize).cumulativeProbability(x.intValue());
            cdfValues.add(cumulativeProbability);
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        // Hypergeometric Distribution does not have parameters to estimate.
        return probabilityDensityFunction(xLabels);
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        // Hypergeometric Distribution does not have parameters to estimate.
        return cumulativeDistributionFunction(xLabels);
    }
}
