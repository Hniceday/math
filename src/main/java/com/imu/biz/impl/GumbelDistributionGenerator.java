package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GumbelDistributionGenerator implements Distribution {

    private double location;  // 位置参数
    private double scale;     // 尺度参数

    public GumbelDistributionGenerator(double location, double scale) {
        this.location = location;
        this.scale = scale;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            double u = Math.random();
            double v = Math.random();

            // Gumbel 分布的生成公式
            double z = location - scale * Math.log(-Math.log(u));

            samples.add(z);
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = (1 / scale) * Math.exp(-(x - location) / scale) * Math.exp(-Math.exp(-(x - location) / scale));
            pdfValues.add(pdf);
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double cdf = Math.exp(-Math.exp(-(x - location) / scale));
            cdfValues.add(cdf);
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        // Gumbel 分布是已知参数的分布，不需要进行估算
        return probabilityDensityFunction(xLabels);
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        // Gumbel 分布是已知参数的分布，不需要进行估算
        return cumulativeDistributionFunction(xLabels);
    }
}
