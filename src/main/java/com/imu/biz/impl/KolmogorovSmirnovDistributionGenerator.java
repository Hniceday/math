package com.imu.biz.impl;

import com.imu.biz.Distribution;
import org.apache.commons.math3.distribution.KolmogorovSmirnovDistribution;


import java.util.ArrayList;
import java.util.List;

public class KolmogorovSmirnovDistributionGenerator implements Distribution {

    private int sampleSize;

    public KolmogorovSmirnovDistributionGenerator(int sampleSize) {
        this.sampleSize = sampleSize;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        // Kolmogorov-Smirnov Distribution does not generate samples,
        // as it is primarily used for statistical testing.
        return new ArrayList<>();
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
//        for (Double x : xLabels) {
//            // Use cumulativeProbability as there is no direct density method
//            double pdf = new KolmogorovSmirnovDistribution(sampleSize).cumulativeProbability(x);
//            pdfValues.add(pdf);
//        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
//        for (Double x : xLabels) {
//            double cumulativeProbability = new KolmogorovSmirnovDistribution(sampleSize).cumulativeProbability(x);
//            cdfValues.add(cumulativeProbability);
//        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        // Kolmogorov-Smirnov Distribution does not have parameters to estimate.
        return probabilityDensityFunction(xLabels);
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        // Kolmogorov-Smirnov Distribution does not have parameters to estimate.
        return cumulativeDistributionFunction(xLabels);
    }
}
