package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PascalDistributionGenerator implements Distribution {

    private int r;        // 成功次数参数
    private double p;     // 成功的概率参数

    public PascalDistributionGenerator(int r, double p) {
        if (r <= 0 || p <= 0 || p >= 1) {
            throw new IllegalArgumentException("r must be positive and p must be between 0 and 1 exclusive.");
        }
        this.r = r;
        this.p = p;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            // Pascal 分布的生成公式
            double sum = 0;
            for (int j = 0; j < r; j++) {
                sum += -Math.log(Math.random()) / Math.log(1 - p);
            }
            samples.add(sum);
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            if (x < 0 || x != Math.floor(x)) {
                pdfValues.add(0.0);  // Pascal 分布在负数和小数的概率为零
            } else {
                double pdf = Math.pow(p, r) * Math.pow(1 - p, x - r) * factorial(x - 1) / (factorial(r - 1) * factorial(x - r));
                pdfValues.add(pdf);
            }
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        double cumulativeProbability = 0;
        for (Double x : xLabels) {
            if (x < 0 || x != Math.floor(x)) {
                cumulativeProbability = 0.0;  // Pascal 分布在负数和小数的累积概率为零
            } else {
                cumulativeProbability += probabilityDensityFunction(Arrays.asList(x)).get(0);
            }
            cdfValues.add(cumulativeProbability);
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        // Pascal 分布是已知参数的分布，不需要进行估算
        return probabilityDensityFunction(xLabels);
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        // Pascal 分布是已知参数的分布，不需要进行估算
        return cumulativeDistributionFunction(xLabels);
    }

    private double factorial(double n) {
        // 计算阶乘的递归函数
        if (n == 0) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }
}
