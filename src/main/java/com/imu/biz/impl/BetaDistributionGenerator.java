package com.imu.biz.impl;

import com.imu.biz.Distribution;
import org.apache.commons.math3.analysis.MultivariateFunction;
import org.apache.commons.math3.distribution.BetaDistribution;
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.PointValuePair;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.SimplexOptimizer;
import org.apache.commons.math3.special.Beta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BetaDistributionGenerator implements Distribution {

    private double alpha;
    private double beta;
    private double alphaEstimate;
    private double betaEstimate;

    public BetaDistributionGenerator(double alpha, double beta) {
        this.alpha = alpha;
        this.beta = beta;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        // 生成 Bates 分布的样本点
        BetaDistribution betaDistribution = new BetaDistribution(alpha, beta);
        double[] data = new double[sampleSize];
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            samples.add(betaDistribution.sample());
            data[i] = samples.get(i);
        }
        double[] estimates = estimateBetaParameters(data);
        this.alphaEstimate = estimates[0];
        this.betaEstimate = estimates[1];
        System.out.println(this.alphaEstimate + "," + this.betaEstimate);
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = new BetaDistribution(alpha, beta).density(x);
            pdfValues.add(pdf);
        }
        return pdfValues;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValues = new ArrayList<>();
        for (Double x : xLabels) {
            double cumulativeProbability = new BetaDistribution(alpha, beta).cumulativeProbability(x);
            cdfValues.add(cumulativeProbability);
        }
        return cdfValues;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        List<Double> pdfValuesEstimate = new ArrayList<>();
        for (Double x : xLabels) {
            double pdf = new BetaDistribution(alphaEstimate, betaEstimate).density(x);
            pdfValuesEstimate.add(pdf);
        }
        return pdfValuesEstimate;
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        List<Double> cdfValuesEstimate = new ArrayList<>();
        for (Double x : xLabels) {
            double cumulativeProbability = new BetaDistribution(alphaEstimate, betaEstimate).cumulativeProbability(x);
            cdfValuesEstimate.add(cumulativeProbability);
        }
        return cdfValuesEstimate;
    }

    public static double[] estimateBetaParameters(double[] data) {
        double[] estimate = new double[2];

        new MaxEval(1000);
        // 初始参数猜测
        double initialAlpha = 2.0;
        double initialBeta = 2.0;

        // 最大似然估计
        BetaDistributionLikelihood likelihood = new BetaDistributionLikelihood(data);
        SimplexOptimizer optimizer = new SimplexOptimizer(1e-6, 1e-6);
        PointValuePair result = optimizer.optimize(
                new MaxEval(200),
                new ObjectiveFunction(likelihood),
                GoalType.MAXIMIZE,
                new InitialGuess(new double[]{initialAlpha, initialBeta})
        );
        estimate[0] = result.getPoint()[0];
        estimate[1] = result.getPoint()[1];
        return estimate;
    }
    // Beta分布的对数似然函数
    static class BetaDistributionLikelihood implements MultivariateFunction {
        private final double[] data;

        BetaDistributionLikelihood(double[] data) {
            this.data = data;
        }

        @Override
        public double value(double[] point) {
            double alpha = point[0];
            double beta = point[1];

            double logLikelihood = 0.0;

            for (double x : data) {
                logLikelihood += (alpha - 1) * Math.log(x) + (beta - 1) * Math.log(1 - x);
            }

            // 添加 Beta 函数的对数
            logLikelihood -= data.length * Beta.logBeta(alpha, beta);

            return -logLikelihood;  // 取负号因为优化器是最小化问题
        }
    }
}



