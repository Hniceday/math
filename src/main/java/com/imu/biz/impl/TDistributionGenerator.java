package com.imu.biz.impl;

import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.List;

public class TDistributionGenerator implements Distribution {

    private double degreesOfFreedom;

    public TDistributionGenerator(double degreesOfFreedom) {
        this.degreesOfFreedom = degreesOfFreedom;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            samples.add(generateSingleSample());
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        return null;
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        return null;
    }

    private Double generateSingleSample() {
        double x = 0.0;
        for (int j = 0; j < degreesOfFreedom; j++) {
            x += Math.pow(Math.random(), 2);
        }
        return Math.random() > 0.5 ? x : -x;
    }
}

