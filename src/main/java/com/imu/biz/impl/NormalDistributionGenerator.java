package com.imu.biz.impl;

// NormalDistribution.java
import com.imu.biz.Distribution;

import java.util.ArrayList;
import java.util.List;

public class NormalDistributionGenerator implements Distribution {

    private double mean;
    private double stdDev;

    public NormalDistributionGenerator(double mean, double stdDev) {
        this.mean = mean;
        this.stdDev = stdDev;
    }

    @Override
    public List<Double> generateSamples(int sampleSize) {
        List<Double> samples = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            double sample = mean + stdDev * Math.sqrt(-2 * Math.log(Math.random())) * Math.cos(2 * Math.PI * Math.random());
            samples.add(sample);
        }
        return samples;
    }

    @Override
    public List<Double> probabilityDensityFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> cumulativeDistributionFunction(List<Double> samples) {
        return null;
    }

    @Override
    public List<Double> estimateProbabilityDensityFunction(List<Double> xLabels) {
        return null;
    }

    @Override
    public List<Double> estimateCumulativeDistributionFunction(List<Double> xLabels) {
        return null;
    }
}

