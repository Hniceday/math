package com.imu.biz;


import com.imu.biz.impl.*;
import com.imu.pojo.DistributionRequest;
import org.apache.commons.math3.distribution.*;

import java.util.List;

// DistributionFactory.java
public class DistributionFactory {

    public static Distribution createDistribution(DistributionRequest request) {
        String distributionType = request.getDistributionType();
        List<Double> parameters = request.getParameters();
        //System.out.println(parameters.get(0)+"======"+parameters.get(1)+"====="+parameters.get(2));
        switch (distributionType) {
            case "ExponentialDistribution":
                return new ExponentialDistributionGenerator(parameters.get(0));
            case "BetaDistribution":
                return new BetaDistributionGenerator(parameters.get(0), parameters.get(1));
            case "BinomialDistribution":
                return new BinomialDistributionGenerator(parameters.get(0).intValue(), parameters.get(1));
            case "CauchyDistribution":
                return new CauchyDistributionGenerator(parameters.get(0), parameters.get(1));
            case "ChiSquaredDistribution":
                return new ChiSquaredDistributionGenerator(parameters.get(0));
            case "ConstantRealDistribution":
                return new ConstantRealDistributionGenerator(parameters.get(0));
            case "EnumeratedDistribution":
                return new EnumeratedDistributionGenerator(parameters.subList(0, parameters.size() / 2),
                        parameters.subList(parameters.size() / 2, parameters.size()));
            case "FDistribution":
                return new FDistributionGenerator(parameters.get(0), parameters.get(1));
            case "GammaDistribution":
                return new GammaDistributionGenerator(parameters.get(0), parameters.get(1));
            case "GeometricDistribution":
                return new GeometricDistributionGenerator(parameters.get(0));
            case "GumbelDistribution":
                return new GumbelDistributionGenerator(parameters.get(0), parameters.get(1));
            case "HypergeometricDistribution":
                return new HypergeometricDistributionGenerator(parameters.get(0).intValue(), parameters.get(1).intValue(),
                        parameters.get(2).intValue());
            case "KolmogorovSmirnovDistribution":
                return new KolmogorovSmirnovDistributionGenerator(parameters.get(0).intValue());
            case "LogisticDistribution":
                return new LogisticDistributionGenerator(parameters.get(0), parameters.get(1));
            case "MultivariateNormalDistribution":
                int dimension = parameters.get(0).intValue();
                double[] mean = new double[dimension];
                double[][] covarianceMatrix = new double[dimension][dimension];
                int index = 1;
                for (int i = 0; i < dimension; i++) {
                    mean[i] = parameters.get(index++);
                    for (int j = 0; j < dimension; j++) {
                        covarianceMatrix[i][j] = parameters.get(index++);
                    }
                }
                return new MultivariateNormalDistributionGenerator(mean, covarianceMatrix);
            case "NakagamiDistribution":
                return new NakagamiDistributionGenerator(parameters.get(0), parameters.get(1));
            case "NormalDistribution":
                return new NormalDistributionGenerator(parameters.get(0), parameters.get(1));
            case "ParetoDistribution":
                return new ParetoDistributionGenerator(parameters.get(0), parameters.get(1));
            case "PascalDistribution":
                return new PascalDistributionGenerator(parameters.get(0).intValue(), parameters.get(1));
            case "PoissonDistribution":
                return new PoissonDistributionGenerator(parameters.get(0));
            case "TDistribution":
                return new TDistributionGenerator(parameters.get(0));
            case "TriangularDistribution":
                return new TriangularDistributionGenerator(parameters.get(0), parameters.get(1), parameters.get(2));
            case "UniformIntegerDistribution":
                return new UniformIntegerDistributionGenerator(parameters.get(0).intValue(), parameters.get(1).intValue());
            case "UniformRealDistribution":
                return new UniformRealDistributionGenerator(parameters.get(0), parameters.get(1));
            case "WeibullDistribution":
                return new WeibullDistributionGenerator(parameters.get(0), parameters.get(1));
            case "ZipfDistribution":
                return new ZipfDistributionGenerator(parameters.get(0).intValue(), parameters.get(1));
            default:
                throw new IllegalArgumentException("Unknown distribution type: " + distributionType);
        }
    }
}

